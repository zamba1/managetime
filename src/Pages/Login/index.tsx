import * as React from 'react';
import useStyle from './style';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { useNavigate } from 'react-router-dom';
import useLoginState from '../GlobalState';
import FormControl from '@material-ui/core/FormControl';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';

export default function LoginPage() {
    const classes = useStyle()
    const history = useNavigate()
    const [email, setEmail] = React.useState('')
    const [password, setPassword] = React.useState('')
    const [showPassword, setShowPassword] = React.useState(false)
    const { loginExecution} = useLoginState()

    const redirectPage = async () => {
      const data = {
        email,
        password
      }
      const newToken = await loginExecution(data)
      console.log({newToken})
      history('/calendar')
    }
    const handleChangeEmail = (event: React.ChangeEvent<HTMLInputElement>) => {
      setEmail(event.target.value);
    };
    const handleChangePassword = (event: React.ChangeEvent<HTMLInputElement>) => {
      setPassword(event.target.value);
    };
    const handleClickShowPassword = () => {
      setShowPassword(!showPassword);
    };
    const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
      event.preventDefault();
    };
  return (
    <div className={classes.rootPage}>
      <Grid container spacing={0} className={classes.card}>
        <Grid item xs={6} className={classes.cardInput}>
          <Typography variant="h4" component="h4">Login</Typography>
          <Typography variant="caption" display="block" style={{color:'grey'}} gutterBottom>
            Manage your time easly
          </Typography>
          <form className={classes.blockInput} noValidate autoComplete="off">
            <TextField className={classes.input} value={email} onChange={handleChangeEmail} id="outlined-basic" label="Email" variant="outlined" />
            <FormControl id="outlined-basic" variant="outlined" className={classes.input}>
              <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
              <OutlinedInput
                id="outlined-adornment-password"
                type={showPassword ? 'text' : 'password'}
                value={password}
                onChange={handleChangePassword}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                }
                labelWidth={70}
              />
              </FormControl>
            <Button variant="contained" color="primary" className={classes.floatRight} onClick={redirectPage}>
              Se connecter
            </Button>
          </form>
        </Grid>
        <Grid item xs={6}>
          <div className={classes.blockBackground}></div>
        </Grid>
      </Grid>
    </div> 
  );
}