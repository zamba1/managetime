import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { convertCompilerOptionsFromJson } from 'typescript';

export default makeStyles((theme: Theme) => ({
    rootPage: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: "100vh",
        width: "100%",
        backgroundColor: "#e9ebede8"
    },
    card: {
        width: "80% !important",
        height: "auto",
        borderRadius: 10,
        boxShadow: '1px 6px 6px -1px rgba(122,111,122,0.3);',
        overflow: 'hidden',
        backgroundColor: "white"
    },
    blockInput: {
        marginTop: 10,
        display: 'block'
    },
    blockBackground: {
        backgroundImage: "url('imgs/login.jpg')",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        width: '100%',
        height: '100%',
        overflow: 'hiden'
    },
    input: {
        marginTop: '10px !important',
        marginBottom: '10px !important',
        position:'relative',
        width: '100%'
    },
    cardInput: {
        paddingTop: 40,
        paddingBottom: 40,
        paddingLeft: 50,
        paddingRight: 50,
    },
    floatRight: {
        marginTop: '10px',
        float: 'right'
    }
}))