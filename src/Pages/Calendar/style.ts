import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme: Theme) => ({
    rootPage: {
        padding: 50,
        backgroundColor: "#e9ebede8",
        height: "100vh",
        display: "block"
    },
    card: {
        width: "100%",
        height: "auto",
        borderRadius: 10,
        boxShadow: '1px 6px 6px -1px rgba(122,111,122,0.3);',
        backgroundColor: "white"
    },
    cardCalendar: {
        width: "100% !important",
        height: "auto",
        borderRadius: 10,
        boxShadow: '1px 6px 6px -1px rgba(122,111,122,0.3);',
        border: 'none !important',
        backgroundColor: "white"
    },
    largeIcon: {
        width: theme.spacing(9),
        height: theme.spacing(9),
    },
}))