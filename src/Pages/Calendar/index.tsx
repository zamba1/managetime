import * as React from 'react';
import useStyle from './style';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { useNavigate } from 'react-router-dom';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import WorkIcon from '@material-ui/icons/Work';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import PhoneIcon from '@material-ui/icons/Phone';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import MessageIcon from '@material-ui/icons/Message';
import MailIcon from '@material-ui/icons/Mail';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css'
import useLoginState from '../GlobalState';

export default function LoginPage() {
  const classes = useStyle()
  const history = useNavigate()
  const [value, onChange] = React.useState(new Date());
  const redirectPage = (path: string) => {
    history(path)
  }
  const token = localStorage.getItem('token')
  const user = JSON.parse(localStorage.getItem('user') || '')
  return (
    <div className={classes.rootPage}>
      <Grid container spacing={5} justifyContent="center">
        <Grid item xs={4}>
          <div className={classes.card}>
            <List>
              <ListItem>
                <ListItemText primary="Overview"/>
                <ListItemSecondaryAction>
                  <Button color="primary" onClick={() => redirectPage('/')}>Logout</Button>
                </ListItemSecondaryAction>
              </ListItem>
              <ListItem>
                <ListItemText primary="eve.holt@reqres.in" secondary="Tana, ambodivona" />
                <ListItemAvatar>
                  <Avatar alt="Toky" src="/imgs/avatar.png" className={classes.largeIcon} />
                </ListItemAvatar>
              </ListItem>
              <ListItem>
                <ListItemAvatar>
                  <Avatar>
                    <WorkIcon />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary="Work" secondary="Software engineer" />
              </ListItem>
              <ListItem>
                <ListItemAvatar>
                  <Avatar>
                    <PhoneIcon />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary={token} secondary="My connexion token" />
              </ListItem>
              <ListItem>
                <ListItemAvatar>
                  <Avatar>
                    <WhatsAppIcon />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary="+261 85 78 888" secondary="WhatsApp" />
              </ListItem>
              <ListItem>
                <ListItemAvatar>
                  <Avatar>
                    <MessageIcon/>
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary="@zamba70" secondary="Messenger" />
              </ListItem>
              <ListItem>
                <ListItemAvatar>
                  <Avatar>
                    <MailIcon />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary={user?.email} secondary="email" />
              </ListItem>
            </List>
          </div>
        </Grid>
        <Grid item xs={5}>
          <Calendar className={classes.cardCalendar} onChange={onChange} value={value} />
        </Grid>
      </Grid>
    </div> 
  );
}