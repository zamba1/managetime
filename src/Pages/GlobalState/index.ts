/* eslint-disable react-hooks/rules-of-hooks */
import { login } from '../../Services/connexion.service';
import { useState, useMemo } from 'react'

interface User {
    email: String,
    password: String
}

const useLoginState = () => {

    const loginExecution = async (data: User) => {
        const resp = await login(data)
        console.log({resp})
        localStorage.setItem('token', resp.data.token)
        localStorage.setItem('user', JSON.stringify(data))
        return resp
    }
    return {
        loginExecution
    }
}
export default useLoginState