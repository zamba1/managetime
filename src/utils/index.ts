import axios from 'axios';

export const makeRequest = async (url: string, method: string, body?: any, headers?: any) => {
    const options: any = {
      method,
      url,
    };
  
    if (body) {
      options.data = body;
    }
  
    if (headers) {
      options.headers = headers;
    }
  
    try {
      return await axios(options);
    } catch (error: any) {
      if (error.message.includes('401')) {
        window.location.replace('/');
      }
  
      throw error;
    }
  };

  export const getHeaders = (authToken?: any) => {
    const headers: any = {
      'Content-Type': 'application/json',
    };
    if (authToken) {
      headers.Authorization = `Bearer ${authToken}`;
    }
    return headers;
  };