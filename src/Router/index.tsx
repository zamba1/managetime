import { LoginPage, CalendarPage } from '../Pages';
import {  
  BrowserRouter,
  Routes,
  Route,
} from 'react-router-dom';

const Router = () => {
  
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<LoginPage />} />
          <Route path="/calendar" element={<CalendarPage />} />
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default Router;
