import { makeRequest, getHeaders } from "../utils";
const baseurl = "https://reqres.in"

export const login = async (data: any, token?: string) => {
    return makeRequest(`${baseurl}/api/login`, 'POST', data, getHeaders(token));
};